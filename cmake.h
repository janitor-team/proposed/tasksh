/* cmake.h.in. Creates cmake.h during a cmake run */

/* Product identification */
#define PRODUCT_TASKSH 1

/* Package information */
#define PACKAGE           "tasksh"
#define VERSION           "1.2.0"
#define PACKAGE_BUGREPORT "support@taskwarrior.org"
#define PACKAGE_NAME      "tasksh"
#define PACKAGE_TARNAME   "tasksh"
#define PACKAGE_VERSION   "1.2.0"
#define PACKAGE_STRING    "tasksh 1.2.0"

#define CMAKE_BUILD_TYPE  ""

/* Localization */
#define PACKAGE_LANGUAGE 
#define LANGUAGE_ENG_USA 

/* git information */
#define HAVE_COMMIT

/* cmake information */
#define HAVE_CMAKE
#define CMAKE_VERSION "3.8.1"

/* Compiling platform */
/* #undef LINUX */
#define DARWIN
/* #undef CYGWIN */
/* #undef FREEBSD */
/* #undef OPENBSD */
/* #undef NETBSD */
/* #undef HAIKU */
/* #undef SOLARIS */
/* #undef KFREEBSD */
/* #undef GNUHURD */
/* #undef UNKNOWN */

/* Found the Readline library */
#define HAVE_READLINE

/* Found the pthread library */
/* #undef HAVE_LIBPTHREAD */

/* Found wordexp.h */
/* #undef HAVE_WORDEXP */

/* Found tm.tm_gmtoff struct member */
/* #undef HAVE_TM_GMTOFF */

/* Found st.st_birthtime struct member */
/* #undef HAVE_ST_BIRTHTIME */

/* Functions */
/* #undef HAVE_GET_CURRENT_DIR_NAME */
/* #undef HAVE_TIMEGM */
/* #undef HAVE_UUID_UNPARSE_LOWER */

